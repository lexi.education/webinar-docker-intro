
# Introducción a Docker
# ![Presentacion 1](icon/docker-banner.png)
## Más información:  [![icon](/icon/link.png)](https://www.docker.com/resources/what-container)
## Contenido

1. #### [Instalación](contenidos/instalacion.md)
2. #### [Post-Instalación](contenidos/post_instalacion.md)
3. #### [Docker Hub](contenidos/comandos.md) 
4. #### [Dockerfile](contenidos/docker_file.md)
5. #### [Docker Compose](contenidos/docker_compose.md)

Creditos al repositorio de https://gitlab.com/diegotony/docky