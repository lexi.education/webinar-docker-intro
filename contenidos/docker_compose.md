# Docker Compose

Permite describir un conjunto de contenedores que se relacionan entre ellos.   Las aplicaciones basadas en microservicios se prestan a usar múltiples contenedores cada uno con un servicio


## Instalación

```
sudo apt install docker-compose
```

## Requisitos
### Dockerfile
```docker
FROM python:3.9.0a5-buster

ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code

RUN pip install -r requirements.txt
```
### docker-compose.yml
```yaml
version: '3'

services: 
    database:
        container_name: db-pg
        image: postgres:11.3
        volumes:
            - './storage:/var/lib/postgresql/data'
    django:
        container_name: proyecto-django
        restart: always
        build: .
        command: python3 manage.py runserver 0.0.0.0:8000
        volumes: 
            - './src:/code'
        ports: 
            - '8001:8000'
        depends_on: 
            - database

```

### requirements.txt

```
 Django == 2.0
 psycopg2

```

## Crear un Proyecto Django
```bash
docker-compose run django django-admin.py startproject projectname .
```


## Crear una app 
```bash
docker-compose run django python manage.py startapp app
```

## Dar permisos para modificar archivos
```bash
sudo chown -R $USER:$USER .
```

## Configurar Postgres 
```python
DATABASES = {
    'default': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'postgres',
    'USER': 'postgres',
    'PASSWORD': '',
    'HOST': 'database',
    'PORT': '5432',
    }
}
```
  
## Migración de Base de datos 
```bash
# Migraciones
docker-compose run django python manage.py migrate
```

## Crear Usuario 
```bash
docker-compose run django python manage.py createsuperuser
```
## Iniciar Servicios
```bash
docker-compose up
```

## Finalizar Servicios
```bash
docker-compose down
```
##### [Retornar a Inicio](../README.md)
