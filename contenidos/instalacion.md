## Instalación Docker
> Probado en Ubuntu

**1. Actualización de repositorios**
```bash
sudo apt-get update
```

**2. Instalación de paquetes necesarios**
```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

**3. Agregar llaves GPG oficiales**
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

**4. Agregamos el repositorio oficial en modo estable**
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
``` 

**5. Actualización de repositorios**
```bash
sudo apt-get update
```

**6. Instalacion de docker**
```bash
sudo apt-get install docker-ce
```

**7. Compruebe que se está ejecutando**
```bash
sudo systemctl status docker
```

## Más información:  [![icon](/icon/link.png)](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
---
##### [Retornar a Inicio](../README.md)