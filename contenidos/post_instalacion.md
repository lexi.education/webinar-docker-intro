
## Configuraciones Post-Instalación 

**1. Agregar el grupo de docker**
```bash
sudo groupadd docker
```

**2. Agregamos nuestro usuario a el grupo de docker que creamos recientemente**
```bash
sudo gpasswd -a "nuestro_usuario" docker
```

**3. Activamos los cambios**
```bash
newgrp docker
```

**4. Corremos un contenedor para probar si la configuracion surgio efecto**
```docker
docker run hello-world
```

## :smiley: **Listo ahora tenemos docker!** :whale:
---
##### [Retornar a Inicio](../README.md)